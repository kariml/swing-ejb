package edu.app.client.delegate;

import java.util.List;

import edu.app.business.EmployeeServiceRemote;
import edu.app.client.locator.ServiceLocator;
import edu.app.persistence.Employee;

public class EmployeeServiceDelegate {
	
	private static String jndiName = "ejb:/prj-ejb/EmployeeService!edu.app.business.EmployeeServiceRemote";
	
	private static EmployeeServiceRemote getEmployeeServiceRemote(){
		return (EmployeeServiceRemote) ServiceLocator.getInstance().getRemoteInterface(jndiName);
	}

	public static void create(Employee employee) {
		getEmployeeServiceRemote().create(employee);
	}

	public static Employee findById(Integer id) {
		return getEmployeeServiceRemote().findById(id);
	}

	public static void update(Employee employee) {
		getEmployeeServiceRemote().update(employee);
	}

	public static void remove(Employee employee) {
		getEmployeeServiceRemote().remove(employee);
	}

	public static List<Employee> findAll() {
		return getEmployeeServiceRemote().findAll();
	}

}

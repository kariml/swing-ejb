package edu.app.client.delegate;


import java.util.List;

import edu.app.business.DepartmentServiceRemote;
import edu.app.client.locator.ServiceLocator;
import edu.app.persistence.Department;

public class DepartmentServiceDelegate {
	
	private static String jndiName = "ejb:/prj-ejb/DepartmentService!edu.app.business.DepartmentServiceRemote";
	
	private static DepartmentServiceRemote getDepartmentServiceRemote(){
		return (DepartmentServiceRemote) ServiceLocator.getInstance().getRemoteInterface(jndiName);
	}

	public static void create(Department department) {
		getDepartmentServiceRemote().create(department);
	}

	public static Department findById(Integer id) {
		return getDepartmentServiceRemote().findById(id);
	}

	public static void update(Department department) {
		getDepartmentServiceRemote().update(department);
	}

	public static void remove(Department department) {
		getDepartmentServiceRemote().remove(department);
	}

	public static List<Department> findAll() {
		return getDepartmentServiceRemote().findAll();
	}

	

}

package edu.app.client.ui;

import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.observablecollections.ObservableCollections;

import edu.app.client.delegate.EmployeeServiceDelegate;
import edu.app.persistence.Employee;
import org.jdesktop.swingbinding.JTableBinding;
import org.jdesktop.swingbinding.SwingBindings;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.Bindings;
import org.jdesktop.beansbinding.ELProperty;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ManageEmployeesForm extends JFrame {
	
	
	private List<Employee> employees;

	private JPanel contentPane;
	private JTextField firstnameField;
	private JTextField lastnameField;
	private JTextField salaryField;
	private JTable masterTable;
	private JButton deleteButton;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ManageEmployeesForm frame = new ManageEmployeesForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ManageEmployeesForm() {
		employees = ObservableCollections.observableList(EmployeeServiceDelegate.findAll());
		setTitle("Manage employees");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 510);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JPanel detailsPanel = new JPanel();
		detailsPanel.setBorder(new TitledBorder(null, "employee details", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		JPanel masterPanel = new JPanel();
		masterPanel.setBorder(new TitledBorder(null, "all employees", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		JPanel actionPanel = new JPanel();
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(masterPanel, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 412, Short.MAX_VALUE)
						.addComponent(actionPanel, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 412, Short.MAX_VALUE)
						.addComponent(detailsPanel, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 412, Short.MAX_VALUE))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
					.addComponent(masterPanel, GroupLayout.DEFAULT_SIZE, 279, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(detailsPanel, GroupLayout.DEFAULT_SIZE, 138, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(actionPanel, GroupLayout.PREFERRED_SIZE, 44, Short.MAX_VALUE))
		);
		
		JButton saveButton = new JButton("save");
		saveButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for(Employee emp : employees){
					EmployeeServiceDelegate.update(emp);
				}
			}
		});
		
		JButton loadButton = new JButton("load");
		loadButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<Employee> loaded = EmployeeServiceDelegate.findAll();
				employees.clear();
				employees.addAll(loaded);
			}
		});
		
		deleteButton = new JButton("delete");
		deleteButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int[] selected = masterTable.getSelectedRows();
				List<Employee> toRemove = new ArrayList<Employee>(selected.length);
				for(int i = 0; i< selected.length; i++){
					Employee emp = employees.get(masterTable.convertRowIndexToModel(selected[i]));
					toRemove.add(emp);
				}
				for(Employee emp : toRemove){
					EmployeeServiceDelegate.remove(emp);
				}
				employees.removeAll(toRemove);
			}
		});
		
		JButton newButton = new JButton("new");
		newButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Employee employee = new Employee();
				employees.add(employee);
				int row = employees.size()-1;
				masterTable.setRowSelectionInterval(row, row);
				masterTable.scrollRectToVisible(masterTable.getCellRect(row, 0, true));
			}
		});
		GroupLayout gl_actionPanel = new GroupLayout(actionPanel);
		gl_actionPanel.setHorizontalGroup(
			gl_actionPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_actionPanel.createSequentialGroup()
					.addContainerGap(20, Short.MAX_VALUE)
					.addComponent(newButton)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(deleteButton)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(loadButton)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(saveButton)
					.addContainerGap())
		);
		gl_actionPanel.setVerticalGroup(
			gl_actionPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_actionPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_actionPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(saveButton)
						.addComponent(loadButton)
						.addComponent(deleteButton)
						.addComponent(newButton))
					.addContainerGap(22, Short.MAX_VALUE))
		);
		actionPanel.setLayout(gl_actionPanel);
		
		JScrollPane masterScrollPane = new JScrollPane();
		GroupLayout gl_masterPanel = new GroupLayout(masterPanel);
		gl_masterPanel.setHorizontalGroup(
			gl_masterPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_masterPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(masterScrollPane, GroupLayout.DEFAULT_SIZE, 392, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_masterPanel.setVerticalGroup(
			gl_masterPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_masterPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(masterScrollPane, GroupLayout.DEFAULT_SIZE, 172, Short.MAX_VALUE)
					.addContainerGap())
		);
		
		masterTable = new JTable();
		masterScrollPane.setViewportView(masterTable);
		masterPanel.setLayout(gl_masterPanel);
		
		JLabel firstnameLabel = new JLabel("firstname");
		
		JLabel lastnameLabel = new JLabel("lastname");
		
		JLabel slaryLabel = new JLabel("salary");
		
		firstnameField = new JTextField();
		firstnameField.setColumns(10);
		
		lastnameField = new JTextField();
		lastnameField.setColumns(10);
		
		salaryField = new JTextField();
		salaryField.setColumns(10);
		GroupLayout gl_detailsPanel = new GroupLayout(detailsPanel);
		gl_detailsPanel.setHorizontalGroup(
			gl_detailsPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_detailsPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_detailsPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(firstnameLabel)
						.addComponent(lastnameLabel)
						.addComponent(slaryLabel))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_detailsPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(salaryField, GroupLayout.DEFAULT_SIZE, 343, Short.MAX_VALUE)
						.addComponent(lastnameField, GroupLayout.DEFAULT_SIZE, 343, Short.MAX_VALUE)
						.addComponent(firstnameField, GroupLayout.DEFAULT_SIZE, 343, Short.MAX_VALUE))
					.addContainerGap())
		);
		gl_detailsPanel.setVerticalGroup(
			gl_detailsPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_detailsPanel.createSequentialGroup()
					.addGap(22)
					.addGroup(gl_detailsPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(firstnameLabel)
						.addComponent(firstnameField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_detailsPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lastnameLabel)
						.addComponent(lastnameField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_detailsPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(slaryLabel)
						.addComponent(salaryField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(81, Short.MAX_VALUE))
		);
		detailsPanel.setLayout(gl_detailsPanel);
		contentPane.setLayout(gl_contentPane);
		initDataBindings();
	}
	protected void initDataBindings() {
		JTableBinding<Employee, List<Employee>, JTable> jTableBinding = SwingBindings.createJTableBinding(UpdateStrategy.READ_WRITE, employees, masterTable);
		//
		BeanProperty<Employee, String> employeeBeanProperty = BeanProperty.create("firstname");
		jTableBinding.addColumnBinding(employeeBeanProperty).setColumnName("FIRSTNAME");
		//
		BeanProperty<Employee, String> employeeBeanProperty_1 = BeanProperty.create("lastname");
		jTableBinding.addColumnBinding(employeeBeanProperty_1).setColumnName("LASTNAME");
		//
		BeanProperty<Employee, Float> employeeBeanProperty_2 = BeanProperty.create("salary");
		jTableBinding.addColumnBinding(employeeBeanProperty_2).setColumnName("SALARY");
		//
		jTableBinding.bind();
		//
		BeanProperty<JTable, String> jTableBeanProperty = BeanProperty.create("selectedElement.firstname");
		BeanProperty<JTextField, String> jTextFieldBeanProperty = BeanProperty.create("text");
		AutoBinding<JTable, String, JTextField, String> autoBinding = Bindings.createAutoBinding(UpdateStrategy.READ_WRITE, masterTable, jTableBeanProperty, firstnameField, jTextFieldBeanProperty);
		autoBinding.bind();
		//
		BeanProperty<JTable, String> jTableBeanProperty_1 = BeanProperty.create("selectedElement.lastname");
		BeanProperty<JTextField, String> jTextFieldBeanProperty_1 = BeanProperty.create("text");
		AutoBinding<JTable, String, JTextField, String> autoBinding_1 = Bindings.createAutoBinding(UpdateStrategy.READ_WRITE, masterTable, jTableBeanProperty_1, lastnameField, jTextFieldBeanProperty_1);
		autoBinding_1.bind();
		//
		BeanProperty<JTable, Float> jTableBeanProperty_2 = BeanProperty.create("selectedElement.salary");
		BeanProperty<JTextField, String> jTextFieldBeanProperty_2 = BeanProperty.create("text");
		AutoBinding<JTable, Float, JTextField, String> autoBinding_2 = Bindings.createAutoBinding(UpdateStrategy.READ_WRITE, masterTable, jTableBeanProperty_2, salaryField, jTextFieldBeanProperty_2);
		autoBinding_2.bind();
	}
}

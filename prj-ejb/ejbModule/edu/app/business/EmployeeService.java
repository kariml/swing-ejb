package edu.app.business;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.app.persistence.Employee;

/**
 * Session Bean implementation class DepartmentService
 */
@Stateless
public class EmployeeService implements EmployeeServiceRemote, EmployeeServiceLocal {

	@PersistenceContext(unitName = "PU01")
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public EmployeeService() {
    }
	@Override
	public void create(Employee employee) {
		em.persist(employee);
		
	}
	@Override
	public Employee findById(Integer id) {
		return em.find(Employee.class, id);
	}
	@Override
	public void update(Employee employee) {
		em.merge(employee);
		
	}
	@Override
	public void remove(Employee employee) {
		em.remove(em.merge(employee));
		
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Employee> findAll() {
		return em.createQuery("select e from Employee e").getResultList();
	}

}

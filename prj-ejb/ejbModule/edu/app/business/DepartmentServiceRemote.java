package edu.app.business;

import java.util.List;

import javax.ejb.Remote;

import edu.app.persistence.Department;

@Remote
public interface DepartmentServiceRemote {
	
	
	
	void create(Department department);
	Department findById(Integer id);
	void update(Department department);
	void remove(Department department);
	List<Department> findAll();

}

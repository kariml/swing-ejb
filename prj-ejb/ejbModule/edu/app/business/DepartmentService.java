package edu.app.business;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.app.persistence.Department;

/**
 * Session Bean implementation class EmployeeService
 */
@Stateless
public class DepartmentService implements DepartmentServiceRemote, DepartmentServiceLocal {

	@PersistenceContext(unitName = "PU01")
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public DepartmentService() {
    }

	@Override
	public void create(Department department) {
		em.persist(department);
		
	}

	@Override
	public Department findById(Integer id) {
		return em.find(Department.class, id);
	}

	@Override
	public void update(Department department) {
		em.merge(department);
		
	}

	@Override
	public void remove(Department department) {
		em.remove(em.merge(department));
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Department> findAll() {
		return em.createQuery("select d from Department d").getResultList();
	}

}

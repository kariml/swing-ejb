package edu.app.business;

import java.util.List;

import javax.ejb.Remote;

import edu.app.persistence.Department;
import edu.app.persistence.Employee;


@Remote
public interface EmployeeServiceRemote {
	
	void create(Employee employee);
	Employee findById(Integer id);
	void update(Employee employee);
	void remove(Employee employee);
	List<Employee> findAll();

}

package edu.app.persistence;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Entity implementation class for Entity: Employee
 *
 */
@Entity
@Table(name="t_employee")

public class Employee implements Serializable {

	
	private Integer id;
	private String firstname;
	private String lastname;
	private Float salary;
	
	private Department department;
	
	private static final long serialVersionUID = 1L;
	

	public Employee() {
	}
	
	


	public Employee(String firstname, String lastname, Float salary) {
		this.firstname = firstname;
		this.lastname = lastname;
		this.salary = salary;
	}




	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		Integer oldId = this.id;
		this.id = id;
		changeSupport.firePropertyChange("id", oldId, id);
	}   
	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		String oldFirstname = this.firstname;
		this.firstname = firstname;
		changeSupport.firePropertyChange("firstname", oldFirstname, firstname);
	}   
	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		String oldLastname = this.lastname;
		this.lastname = lastname;
		changeSupport.firePropertyChange("lastname", oldLastname, lastname);
	}
	public Float getSalary() {
		return this.salary;
	}

	public void setSalary(Float salary) {
		Float oldSalary = this.salary;
		this.salary = salary;
		changeSupport.firePropertyChange("salary", oldSalary, salary);
	}



	@ManyToOne
	@JoinColumn(name = "department_fk")
	public Department getDepartment() {
		return department;
	}




	public void setDepartment(Department department) {
		Department oldDepartment = this.department;
		this.department = department;
		changeSupport.firePropertyChange("department", oldDepartment, department);
	}




	




	@Override
	public String toString() {
		return "Employee [id=" + id + ", firstname=" + firstname
				+ ", lastname=" + lastname + ", salary=" + salary + "]";
	}
	
	@Transient
	public PropertyChangeSupport getChangeSupport() {
		return changeSupport;
	}




	public void setChangeSupport(PropertyChangeSupport changeSupport) {
		this.changeSupport = changeSupport;
	}

	private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
	
	public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
	
	
	
   
}
